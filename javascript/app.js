'use strict'
// Trust the intermediate CA for influx-cewit to prevent TLS errors.
const rootCas = require('ssl-root-cas/latest').create();
rootCas.addFile('influx-cewit-intermediate.pem');
require('https').globalAgent.options.ca = rootCas;

const Influx = require('influx');
const os = require('os');

// Influx DB Connection Properties
const INFLUX_HOST = 'influx-cewit.netsmartdev.com';
const INFLUX_DATABASE = '';
const INFLUX_USER = '';
const INFLUX_PASS = '';

// Setup the Database Connection
const influx = new Influx.InfluxDB(
    {
        host: INFLUX_HOST,
        port: 443,
        username: INFLUX_USER,
        password: INFLUX_PASS,
        protocol: 'https',
        database: INFLUX_DATABASE,
        schema: [
            {
                // Replace this with your measurement(s)
                measurement: 'request_timer',
                tags: ['component', 'type', 'user', 'os', 'hostname'],
                fields: {
                    // Specify the Field Type Here so the Influx SDK knows how to coerce the data properly.
                    responseTimeMs: Influx.FieldType.INTEGER,
                    requestSizeBytes: Influx.FieldType.INTEGER
                }
            }
        ]
    }
);

// Write 10 points with a 2 second interval
for (let i = 1; i <= 10; i++) {
    setTimeout(() => {
        writePoint()
    }, 2000 * i);
}

/**
 * Write one point to the Influx DB
 */
function writePoint() {
    const nowInNanoSeconds = (Date.now() * 1000000).toString();
    influx.writePoints([
        {
            measurement: 'request_timer',
            timestamp: Influx.toNanoDate(nowInNanoSeconds),  // This can also be left off to specify 'now'
            fields: {
                responseTimeMs: getResponseTime(),
                requestSizeBytes: getRequestByteSize()
            },
            tags: {
                component: 'primary_web_app',
                type: 'method_timer',
                user: os.userInfo().username,
                os: os.platform() + ' ' + os.release(),
                hostname: os.hostname()
            }
        }
    ]).then(() => {
        console.log('[info] Wrote Point to Influx Db @ ' + INFLUX_HOST + '/' + INFLUX_DATABASE);
    }).catch(reason => {
        console.log('[ERROR] Failed to write point: ' + reason);
    });
}

/**
 * Generate a random integer within the specified interval
 * @param min - minimum number
 * @param max - maximum number
 * @return {number} The random integer
 */
function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * Generate a random response time between 50 and 10000
 * @return {number} The random integer
 */
function getResponseTime() {
    return randomIntFromInterval(50, 10000);
}

/**
 * Generate a random byte size between 1024 and 10000
 * @return {number} The random integer
 */
function getRequestByteSize() {
    return randomIntFromInterval(1024, 10000);
}
