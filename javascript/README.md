### Requirements
Node.js 6+

Run `npm install` from the `javascript` directory once.

### To Run
Add your team's credentials to the `INFLUX_DATABASE`, `INFLUX_USER` and `INFLUX_PASS` constants

Execute `npm start`
