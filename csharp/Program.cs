﻿using InfluxData.Net.InfluxDb;
using InfluxData.Net.InfluxDb.Models;
using System;
using System.Collections.Generic;

namespace csharp
{
    /// <summary>
    /// NETSMART EXAMPLE CODE FOR HACK@CEWIT
    /// </summary>
    public class Program
    {
        private static string _influxHost = "https://influx-cewit.netsmartdev.com:443/";
        private static string _influxDatabase = "";
        private static string _influxUser = "";
        private static string _influxPass = "";

        /// <summary>
        /// Main entrypoint for the application
        /// </summary>
        /// <param name="args">command line arguments</param>
        static void Main(string[] args)
        {
            _influxDatabase = args.Length > 0 ? args[0] : _influxDatabase;
            _influxUser = args.Length > 1 ? args[1] : _influxUser;
            _influxPass = args.Length > 2 ? args[2] : _influxPass;

            InsertDataPoints(_influxDatabase, _influxUser, _influxPass);

            // Read line so console stays open for debugging
            Console.ReadLine();
        }

        /// <summary>
        /// Creates 10 random data points and writes them to the Influx Database
        /// </summary>
        /// <param name="database">The Influx database to use</param>
        /// <param name="user">The Influx username</param>
        /// <param name="password">The Influx password</param>
        private static void InsertDataPoints(string database, string user, string password)
        {
            try
            {
                InfluxDbClient influxDbClient = new InfluxDbClient(_influxHost, user, password, InfluxData.Net.Common.Enums.InfluxDbVersion.Latest);

                // Create tags
                Dictionary<string, object> tags = new Dictionary<string, object>();
                tags.Add("component", "primary_web_app");
                tags.Add("type", "method_timer");
                tags.Add("user", Environment.UserName);
                tags.Add("os", Environment.OSVersion.VersionString);
                tags.Add("hostname", Environment.MachineName);

                // Send 10 random metric values into the database
                for (int i = 0; i < 10; i++)
                {
                    Random random = new Random();

                    // Random response time
                    int responseTime = random.Next(50, 10000);

                    // Random request size
                    int payloadSize = random.Next(1024, 10000);

                    // Create fields
                    Dictionary<string, object> fields = new Dictionary<string, object>();
                    fields.Add("responseTimeMs", responseTime);
                    fields.Add("requestSizeBytes", payloadSize);

                    // Create point
                    Point point = new Point()
                    {
                        Name = "request_timer",
                        Timestamp = DateTime.UtcNow,
                        Tags = tags,
                        Fields = fields
                    };

                    // Write point
                    try
                    {
                        influxDbClient.Client.WriteAsync(point, database).Wait();
                        Console.WriteLine($"[info] Wrote point to InfluxDB {_influxHost}/{database}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"[error] Failed Point {ex.Message}");
                    }

                    // Sleep 2 seconds
                    System.Threading.Thread.Sleep(2000);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"[error] {ex.Message}");
            }
        }
    }
}
