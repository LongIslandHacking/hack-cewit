![Netsmart](https://www.ntst.com/-/media/logos/Netsmart/logo-netsmart-color.svg) 
# Welcome to Netsmart's Hack@CEWIT Monitoring Service!!!

We love monitoring our solutions, and we hope you will too!  

## Overview
Here you will learn to instrument your code, with [InfluxDB](https://www.influxdata.com/get-influxdb/), storing time series data on anything you want in your application!

With the [Telegraf agent](https://github.com/influxdata/telegraf) you can monitor traditional system metrics, and even metrics on services you use like databases and web servers!  Telegraf will push time series to the same database where you store your app metrics.  

Learning to use [Grafana](https://grafana.com/grafana/) will allow you one single place for your application, service, and system metrics.

We want you focused on applying monitoring techniques, so we at Netsmart Engineering have configured an InfluxDB and Grafana instance already in AWS for you. 

Tonight's training will be focused on ensuring you learn the skills needed to ship solutions with monitoring.  

We will help to ensure...

* Your pre-generated connection details to InfluxDB and Grafana work. 
* You are able to take any example, update the variables with your connection information and execute it, to visualize the results within Grafana.
* Have a new found love of monitoring!


Wherever your journey takes you, we hope you will **remember us**, and **most importantly**, remember that **instrumenting early will pay dividends** in the future. 

Every organization will be in different stages of implementing DevOps practices. Adding a little bit of code goes a long way to providing the visiblity that you and your fellow engineers need to successfully own a solution.

-Netsmart

### Grafana Instance: [https://grafana-cewit.netsmartdev.com](https://grafana-cewit.netsmartdev.com)
### InfluxDB Instance: [https://influxdb-cewit.netsmartdev.com](https://influxdb-cewit.netsmartdev.com) 

#### P.S. We're Hiring!  Want to learn more? Find us during the Hackathon, hit us up on our [CEWIT Slack Channel](https://hackatcewit.slack.com/archives/CTNPG2M5K) or [visit our Careers Site](https://www.ntst.com/About-Us/Careers)
---

## Code Examples

Within this repository you will find code examples in multiple languages.  Each code example can be re-used in your solutions, to monitor anything you wish. These examples assume two things.

- Your solution has outbound HTTPS connectivity 
- The influx instance is listening on port 443

### Languages

- C#
- Java
- JavaScript
- Python

---

## Requirements

In order to fully take advantage of the training we suggest you come ready with the following.

- A laptop (Windows, Linux, or MacOS)
- A browser (Edge Chromium, Google Chrome, Safari)
- A code editor installed on said laptop, we will be conducting the examples using [VS Code](https://code.visualstudio.com/)
- A SDK or Runtime for one the languages mentioned above.

---

## Clone a repository

Use these steps to clone from SourceTree, the git client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
