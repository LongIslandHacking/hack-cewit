/*
 *  NETSMART EXAMPLE CODE FOR HACK@CEWIT
 */
package com.ntst.hack.cewit;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

public class InfluxDBInsert {

    private final static String INFLUX_HOST = "https://influx-cewit.netsmartdev.com";
    private static String INFLUX_DATABASE = "";
    private static String INFLUX_USER = "";
    private static String INFLUX_PASS = "";

    /**
     * Main entrypoint for the application
     *
     * @param args
     *     command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        INFLUX_DATABASE = args.length > 0 ? args[0] : INFLUX_DATABASE;
        INFLUX_USER = args.length > 1 ? args[1] : INFLUX_USER;
        INFLUX_PASS = args.length > 2 ? args[2] : INFLUX_PASS;
        insertDataPoints(INFLUX_DATABASE, INFLUX_USER, INFLUX_PASS);
    }

    /**
     * Attempt to resolve the current machine's hostname.
     *
     * @return The current machine's hostname or 'UNKNOWN'
     */
    private static String getHostName() {
        String host;
        try {
            host = InetAddress.getLocalHost().getHostName();
        } catch (Exception ex) {
            host = "UNKNOWN";
        }
        return host;
    }

    /**
     * Creates 10 random data points and writes them to the Influx Database
     *
     * @param database
     *     The Influx database to use
     * @param user
     *     The Influx username
     * @param password
     *     The Influx password
     */
    private static void insertDataPoints(String database, String user, String password) throws InterruptedException {
        try (InfluxDB influxDB = InfluxDBFactory.connect(INFLUX_HOST, user, password)) {

            // Set up exception handler
            influxDB.enableBatch(BatchOptions.DEFAULTS.exceptionHandler(
                (failedPoints, throwable) -> {
                    for (Point failedPoint : failedPoints) {
                        System.err.println("[ERROR] Failed Point: " + failedPoint);
                    }
                    System.err.println(throwable);
                })
            );

            // Set Database
            influxDB.setDatabase(database);

            // Create tags
            Map<String, String> tags = new HashMap<>();
            tags.put("component", "primary_web_app");
            tags.put("type", "method_timer");
            tags.put("user", System.getProperty("user.name"));
            tags.put("os", System.getProperty("os.name"));
            tags.put("hostname", InfluxDBInsert.getHostName());

            // Send 10 random metric values into the database
            for (int i = 0; i < 10; i++) {

                // Random response time
                int responseTime = ThreadLocalRandom.current().nextInt(50, 10000);

                // Random request size
                int payloadSize = ThreadLocalRandom.current().nextInt(1024, 10000);

                // Create Point
                Point point = Point.measurement("request_timer")
                    .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                    .tag(tags)
                    .addField("responseTimeMs", responseTime)
                    .addField("requestSizeBytes", payloadSize)
                    .build();

                // Write Point
                influxDB.write(point);
                System.out.println("[info] Wrote Point to Influx Db @ " + INFLUX_HOST + "/" + INFLUX_DATABASE);

                Thread.sleep(2000);
            }
        }
    }

}
