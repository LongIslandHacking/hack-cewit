## Add Influx Cert to your Keystore 
Execute this command if you receive a PKIX Certificate error when connecting to Influx.

`sudo keytool -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit -noprompt -trustcacerts -importcert -alias influx-cewit-netsmartdev-com -file influx-cewit-netsmartdev-com.pem`

### To Run
`mvn clean package`

`java -jar target/hack-cewit-java-1.0-SNAPSHOT.jar <DBNAME> <USERNAME> <PASSWORD>`
